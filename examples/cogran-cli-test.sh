set -e
set -x
set -u

# These are the same example test-cases as on https://github.com/funkeinteraktiv/cogran
# and as in the (rejected) paper (2018-02)
# 2m11s for gpkg
# 2m for shp

verbosity="INFO"

time scripts/cogran_cli.py \
--method areal_weighting \
--source testdata/berlin/kriminalitaet_bezirksregionen.geojson \
--target testdata/berlin/447_lor_planungsraeume.geojson \
--attribute Alle_2012 \
--output /tmp/areal_weighting.gpkg --driver GPKG \
--verbosity "${verbosity}"

time scripts/cogran_cli.py \
--method areal_weighting \
--source testdata/hamburg/wohnflaecheProEW_Stadtteile.geojson \
--target testdata/hamburg/7_bezirke.geojson \
--attribute WFl_m2 \
--relative True \
--output /tmp/areal_weighting_relative.gpkg --driver GPKG \
--verbosity "${verbosity}"

time scripts/cogran_cli.py \
--method attribute_weighting \
--source testdata/berlin/kriminalitaet_bezirksregionen.geojson \
--target testdata/berlin/zugezogene_planungsraeume.geojson \
--attribute Alle_2012 \
--weight Einwohner \
--output /tmp/attribute_weighting.gpkg --driver GPKG \
--verbosity "${verbosity}"

time scripts/cogran_cli.py \
--method attribute_weighting \
--source testdata/hamburg/wohnflaecheProEW_Stadtteile.geojson \
--target testdata/hamburg/einwohner_bezirke.geojson \
--attribute WFl_m2 \
--weight Insgesamt \
--relative True \
--output /tmp/attribute_weighting_relative.gpkg --driver GPKG \
--verbosity "${verbosity}"

time scripts/cogran_cli.py \
--method binary_dasymetric_weighting \
--source testdata/berlin/kriminalitaet_bezirksregionen.geojson \
--target testdata/berlin/447_lor_planungsraeume.geojson \
--attribute Alle_2012 \
--mask testdata/berlin/wohnbloecke.geojson \
--output /tmp/binary_dasymetric_weighting.gpkg --driver GPKG \
--verbosity "${verbosity}"

time scripts/cogran_cli.py \
--method binary_dasymetric_weighting \
--source testdata/hamburg/wohnflaecheProEW_Stadtteile.geojson \
--target testdata/hamburg/7_bezirke.geojson \
--attribute WFl_m2 \
--mask testdata/hamburg/bebauteFlaeche.geojson \
--relative True \
--output /tmp/binary_dasymetric_weighting_relative.gpkg --driver GPKG \
--verbosity "${verbosity}"

time scripts/cogran_cli.py \
--method nclass_dasymetric_weighting \
--source testdata/berlin/kriminalitaet_bezirksregionen.geojson \
--target testdata/berlin/447_lor_planungsraeume.geojson \
--attribute Alle_2012 \
--mask testdata/berlin/wohnbloecke.geojson \
--weight percentage \
--output /tmp/nclass_dasymetric_weighting.gpkg --driver GPKG \
--verbosity "${verbosity}"

time scripts/cogran_cli.py \
--method nclass_dasymetric_weighting \
--source testdata/hamburg/wohnflaecheProEW_Stadtteile.geojson \
--target testdata/hamburg/7_bezirke.geojson \
--attribute WFl_m2 \
--mask testdata/hamburg/einwohner_statistischeGebiete.geojson \
--weight percentage \
--relative True \
--output /tmp/nclass_dasymetric_weighting_relative.gpkg --driver GPKG \
--verbosity "${verbosity}"
