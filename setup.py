import sys
import logging
from setuptools import setup, find_packages

logging.basicConfig(stream=sys.stderr, level=logging.INFO)
log = logging.getLogger()

# by reading requirements.txt we have both a local setup.py installation AND
# a pip install consider the same packages. pip only looks at install_requires.
with open("requirements.txt") as f:
    requirements = f.read().strip().split("\n")

with open("README.md", encoding='utf-8') as f:
    long_description = f.read()

setup(
    url="https://gitlab.com/g2lab/cogran-python",
    name="cogran",
    version="0.3",
    author="g2lab, HafenCity Universität Hamburg",
    author_email="g2lab@hcu-hamburg.de",
    description="CoGran is a tool for combining data of different spatial granularity",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Scientific/Engineering :: GIS",
    ],
    entry_points={
        'console_scripts': [
            'cogran = cogran.cli.cogran_cli:main',
            'cogran-gui = cogran.gui.cogran_gui:main'
        ],
        'gui_scripts': [
            'cogran-gui = cogran.gui.cogran_gui:main'
        ]
    },
    python_requires=">=3.6",
    install_requires=requirements,
    packages=find_packages(),
)

log.info("To run the cogran and cogran_gui tools make sure they are in your PATH.")
