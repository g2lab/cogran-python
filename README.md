# CoGran

CoGran is a tool for **co**mbining data of different spatial **gran**ularity. You can find CoGran on [Gitlab](https://gitlab.com/g2lab/cogran-python).

CoGran allows a spatial re-organization of geographical data containing quantitative (statistical) information due to the fact that this is often not given in identical spatial units (like postal code districts, wards, or urban districts). Based on several different methods correlations between e.g. election results and income can be visualized.

CoGran is available as a Python package. A command-line interface and a GUI are included.

![Image of Example](docs/images/postcodes%20vs%20districts.png?raw=true)

## Supported Methods

### Areal Weighting
Weights the attribute value by the area of intersection between source and target file.

![Schematic of Areal Weighting](docs/images/Areal%20Weighting.png?raw=true)

### Attribute Weighting
Weights the attribute value by an additional attribute of the target file (e.g. population).

![Schematic of Attribute Weighting](docs/images/Attribute%20Weighting.png?raw=true)

### Binary Dasymetric Weighting
Areal Weighting but with an additional input file representing a binary mask for the source features. Instead of whole source feature areas, only the spatial intersection of source and mask features will be considered.

![Schematic of Binary Dasymetric Weighting](docs/images/Binary%20Dasymetric%20Weighting.png?raw=true)

### N-Class Dasymetric Weighting
Dasymetric mapping where an attribute of the mask features is used to further weight the calculations.

![Schematic of N-Class Dasymetric Weighting](docs/images/N-Class%20Dasymetric%20Weighting.png?raw=true)

## Using CoGran

Before you start, a **warning**: If you use big and complex data sets, CoGran might take a long time to finish and use a lot of RAM. "Big and complex" starts in a low six digit number of features.

### Using the Python package

For example:

```
import cogran
from cogran.helpers import get_features, to_file

# load features from input datasets
source_features, source_meta = get_features("source.gpkg")
target_features, target_meta = get_features("target.gpkg")

# prepare geometries and spatial index
source_geometries = cogran.make_geometries(source_features)
target_geometries = cogran.make_geometries(target_features)

source_idx = cogran.generate_strtree(source_geometries)

# specify the attribute to be re-distributed into the target zones
attribute = "foo"

# run CoGran
target_features = cogran.areal_weighting(
    source_features=source_features,
    source_geometries=source_geometries,
    source_idx=source_idx,
    target_features=target_features,
    target_geometries=target_geometries,
    attribute=attribute
)

# write the result to a file
to_file(
    target_features,
    target_meta,
    attribute,
    "result.gpkg",
    driver="GPKG",
)
```

Note: CoGran will not mutate the source features but will add the re-distributed attribute values to the target features.

### Using the `cogran` command-line interface

See `cogran --help`.

For example:

```
cogran \
--method areal_weighting \
--source source.shp \
--target target.geojson \
--attribute foo \
--output result.gpkg --driver GPKG
```

### Using the CoGran GUI

Run `cogran-gui` and follow its interactive instructions.

![Screenshot of GUI](docs/images/gui.png?raw=true)

## Installation

### From Source

You can install the latest release using pip:

```
pip install [--user] cogran
```

Or, if needed, you can install the latest development version from GitLab using pip:

```
pip install [--user] git+https://gitlab.com/g2lab/cogran-python
```

#### Dependencies

CoGran requires Python 3.6+. Its Python package dependencies are `click`, `fiona`, `PyQt5`, `qtmodern`, `shapely`, `tqdm`.

For Windows users the installation using pre-built binaries from https://www.lfd.uci.edu/~gohlke/pythonlibs/ is highly recommended.

Another alterative (especially for windows...) is the usage of conda environments via [Miniconda3](https://docs.conda.io/en/latest/miniconda.html). Then `conda-env.yml` needs to be used in the following way:

```
conda env create -f conda-env.yml
```
This will automatically create a closed environment with the name `cogran-python` which has all binary dependencies installed. It then needs to be activated with `conda activate cogran-python`. Then you still need to install cogran with pip like describe above.

### GUI executable

The GUI will be available as self-contained binary for Windows.

## Development

You can use `pip install -e .` to install the package in live editing mode. This method will direct any use of the code to the current version living where you invoked this command.

For development you need additional requirements which are specified in ``requirements-dev.txt``. If you are using a conda environment use the `conda-env-dev.yml` and the command `conda env create -f conda-env-dev.yml`.

If you are writing a new feature, please try to use a feature branch accordingly.

Please try to use an appropriate prefix for your commits, something like "cli", "doc", "gui", "module", "packaging" or "tests". Just look at the commit history.

For testing [pytest](https://docs.pytest.org/en/latest/) is strongly recommended, though the tests are still written with the unittest package. All new tests should be written in pytest-style. To run the tests simply write `pytest` in the commandline.

For a clean repository the usage of pre-commit is mandatory. At the moment black and flake8 are used to ensure that we stay in line with PEP8 and keep the code as readable as possible. Install pre-commit hooks with the following command `pre-commit install`. Now everytime you commit pre-commit runs and blocks the current commit until the used hooks do not report any erros.

## Credits

CoGran is a tool for combining data of different spatial granularity.

Copyright © g2lab, HafenCity Universität Hamburg (HCU)

Contributors (in alphabetical order):

- Johannes Kröger, HCU
- Philipp Loose, HCU
- Helge Schneider, HCU

Based on the JavaScript/node.js version https://github.com/funkeinteraktiv/cogran, originally developed by HafenCity Universität Hamburg and Berliner Morgenpost, funded by a grant of Volkswagen-Stiftung.

Contributors (in alphabetical order):

- Christopher Möller, Berliner Morgenpost
- Jochen Schiewe, HCU
- Inga Schlegel, HCU
- Julius Tröger, Berliner Morgenpost
