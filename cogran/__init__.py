"""CoGran is a tool for combining data of different spatial granularity.

CoGran allows a spatial re-organization of geographical data containing quantitative
(statistical) information due to the fact that this is often not given in identical
spatial units (like postal code districts, wards, or urban districts). Based on
several different methods correlations between e.g. election results and income
can be visualized.
"""
from copy import deepcopy
import logging
from math import isclose

from shapely.geometry import mapping
from shapely.ops import unary_union

from tqdm.auto import tqdm

from cogran.helpers import get_features, make_geometries, make_valid, generate_strtree


def is_pycnophylactic(source_features, target_features, attribute, rel_tol=0.001):
    """Determine the pycnophylactic property.

    The total sum of the values of the chosen attribute should be
    the same between source and target.

    Note: A relative error of 0.0001, which corresponds to 0.1%, is used as
          default here (*for arbitrary reasons right now)

    Args:
        source_features (sequence): The source features.
        target_features (sequence): The target features.
        attribute (str): An attribute to compare.
        rel_tol (float): A relative tolerance for isclose(). Defaults to 0.1%.

    Returns:
        bool: True if pycnophylactic.

    """
    source_sum = sum([f["properties"][attribute] for f in source_features])
    target_sum = sum([f["properties"][attribute] for f in target_features])

    pycnophylactic = isclose(source_sum, target_sum, rel_tol=rel_tol)

    logging.info(
        (
            f"Pycnophylactic property: {source_sum} (source) is"
            f"{'' if pycnophylactic else ' NOT'} within "
            f"{rel_tol}% of {target_sum} (target)."
        )
    )

    return pycnophylactic


def check_pycnophylactic(
    source_features, target_features, attribute, ignore_pycnophylactic=False
):
    """Check the pycnophylactic property.

    Can be used to trigger a NotPycnophylacticError if needed.

    Args:
        source_features (sequence): The source features.
        target_features (sequence): The target features.
        attribute (str): An attribute to compare.
        ignore_pycnophylactic (bool):  Boolean flag indicating if the pycnophylactic
            property should be asserted. Defaults to False.

    Raises:
         NotPycnophylacticError: If not pycnophylactic.

    """
    if not is_pycnophylactic(source_features, target_features, attribute):
        message = (
            "Not pycnophylactic! Sum of attribute values in sources does not "
            "match output!"
        )

        if ignore_pycnophylactic:
            logging.warning(message)
        else:
            raise NotPycnophylacticError(message)


def areal_weighting(**kwargs):
    """Perform area weighting.

    For every target-feature the area of intersection (Ast) with the intersecting
    source-feature is calculated and set in proportion to the source-feature area
    (As) (relative values: target-feature area (At)).

    The resulting quotient is multiplied by the specified attribute value (Ps) of
    the source-feature and the result (Pt) is then summed up for all intersecting
    source features per target feature.

    Any STRTree passed to this function must have its geometries tagged with their
    original index number, see generate_strtree().

    Args:
        **kwargs: Mandatory and optional keyword arguments:
            source_features (sequence): The source features. Mandatory.
            source_geometries (sequence): The source geometries. Mandatory.
            source_idx: STRTree index for the source geometries. Mandatory.
            target_features (sequence): The target features. Mandatory.
            target_geometries (sequence): The target geometries. Mandatory.
            attribute (str): The attribute (of the source features) to use.
                Mandatory.
            relative (bool): Boolean flag indicating if attribute should be
                considered relative. Defaults to False. Optional.
            ignore_pycnophylactic: Boolean flag indicating if the pycnophylactic
                property should be ignored. Defaults to False. Optional.


    Returns:
        list: Target features with their newly calculated attribute values.

    """
    logging.info("Running Areal Weighting...")

    source_features = kwargs["source_features"]
    source_geometries = kwargs["source_geometries"]
    source_idx = kwargs["source_idx"]
    target_features = kwargs["target_features"]
    target_geometries = kwargs["target_geometries"]
    attribute = kwargs["attribute"]
    relative = kwargs.get("relative")
    ignore_pycnophylactic = kwargs.get("ignore_pycnophylactic")

    logging.info("Processing target features...")
    for i, target in enumerate(tqdm(target_features)):

        Pt = 0

        target_geom = target_geometries[i]

        for hit in source_idx.query(target_geom):

            source_feature = source_features[hit.i]
            source_value = source_feature["properties"][attribute]
            source_geom = source_geometries[hit.i]

            intersection = source_geom.intersection(target_geom)
            intersection = make_valid(intersection)

            if not intersection.is_empty:

                Ps = source_value
                As = source_geom.area

                At = target_geom.area

                Ast = intersection.area

                if not relative:
                    Pt += (Ast / As) * Ps
                else:
                    Pt += (Ast / At) * Ps

        # add new field for source attribute in target feature
        # and assign computed value
        target["properties"][attribute] = Pt

    if not relative:
        check_pycnophylactic(
            source_features, target_features, attribute, ignore_pycnophylactic
        )

    return target_features


def attribute_weighting(**kwargs):
    """Perform attribute weighting.

    At first the specified weight (Qt) of the target-features has to be
    calculated (Qst) and summed up for every intersecting source feature
    (Qs).

    For this purpose for every target-feature the intersection area (Ast) of
    source- and target-features is related to the intersecting target area
    (At) and multiplied by the specified target-feature weight (Qt). The
    result (Qst) is appended to a python-list of Qs-values of the inter-
    secting source feature.

    The second step is to use the calculated Qs value of the single source-
    features in the final calculation.

    To achieve this Qst is calculated again for every target-source-feature
    intersection (Qst = (Ast / At) * Qt). To calculate the new target-feature
    attribute value (Pt) Qst is now related to the sum of Qs-weight-values per
    intersecting source feature (relative values: target weight (Qt)) and
    multiplied by the specified attribute value (Ps). The result of this
    calculation is again summed up over all intersecting source features per
    target feature.

    Any STRTree passed to this function must have its geometries tagged with their
    original index number, see generate_strtree().

    Args:
        **kwargs: Mandatory and optional keyword arguments:
            source_features (sequence): The source features. Mandatory.
            source_geometries (sequence): The source geometries. Mandatory.
            source_idx: STRTree index for the source geometries. Mandatory.
            target_features (sequence): The target features. Mandatory.
            target_geometries (sequence): The target geometries. Mandatory.
            attribute (str): The attribute (of the source features) to use.
                Mandatory.
            weight (str): The weight attribute (of the target features) to use.
                Mandatory.
            relative (bool): Boolean flag indicating if attribute should be
                considered relative. Defaults to False. Optional.
            ignore_pycnophylactic: Boolean flag indicating if the pycnophylactic
                property should be ignored. Defaults to False. Optional.

    Returns:
        list: Target features with their newly calculated attribute values.

    """
    logging.info("Running Attribute Weighting...")

    source_features = kwargs["source_features"]
    source_geometries = kwargs["source_geometries"]
    source_idx = kwargs["source_idx"]
    target_features = kwargs["target_features"]
    target_geometries = kwargs["target_geometries"]
    attribute = kwargs["attribute"]
    weight = kwargs["weight"]
    relative = kwargs.get("relative")
    ignore_pycnophylactic = kwargs.get("ignore_pycnophylactic")

    for f in source_features:
        f["properties"]["Qs"] = []

    logging.info("Calculating target weight per source feature...")
    for i, target in enumerate(tqdm(target_features)):

        target_geom = target_geometries[i]
        target_value = target["properties"][weight]

        for hit in source_idx.query(target_geom):

            source_feature = source_features[hit.i]
            source_value = source_feature["properties"][attribute]
            source_geom = source_geometries[hit.i]

            intersection = source_geom.intersection(target_geom)
            intersection = make_valid(intersection)

            Qs = source_feature["properties"]["Qs"]

            if not intersection.is_empty:
                At = target_geom.area
                Qt = target_value

                Ast = intersection.area

                Qst = (Ast / At) * Qt
                Qs.append(Qst)

    logging.info("Processing target features...")
    for i, target in enumerate(tqdm(target_features)):

        Pt = 0

        target_geom = target_geometries[i]
        target_value = target["properties"][weight]

        for hit in source_idx.query(target_geom):

            source_feature = source_features[hit.i]
            source_Qs = source_feature["properties"]["Qs"]
            source_value = source_feature["properties"][attribute]
            source_geom = source_geometries[hit.i]

            intersection = source_geom.intersection(target_geom)
            intersection = make_valid(intersection)

            if not intersection.is_empty:

                Qs = sum(source_Qs)

                Ps = source_value

                At = target_geom.area
                Qt = target_value

                Ast = intersection.area

                Qst = (Ast / At) * Qt

                if not relative:
                    Pt += (Qst / Qs) * Ps
                else:
                    Pt += (Qst / Qt) * Ps

        target["properties"][attribute] = Pt

    if not relative:
        check_pycnophylactic(
            source_features, target_features, attribute, ignore_pycnophylactic
        )

    return target_features


def binary_dasymetric_weighting(**kwargs):
    """Perform binary dasymetric weighting.

    In the first step the source features are masked, meaning the area of the
    source-features is reduced to that of the resulting intersection with the
    mask while maintaining the sources' attribute values.

    Within this step a single source feature can happen to be  split up into
    multiple single features all referring to the prior source-feature. For
    this case these single features are dissolved into one single masked
    source-feature.

    The next step considers the intersection between the masked source-features
    and the given target-features.

    Like with simple area weighting for every target-feature the intersection
    area (Asct) is related to the intersecting masked source-feature area
    (Asc) (relative values: target-feature area (At)). The resulting quotient
    is multiplied by the specified attribute value (Ps) of the source-feature
    and the result (Pt) is then summed up for all intersecting source-
    features per target feature.

    Any STRTree passed to this function must have its geometries tagged with their
    original index number, see generate_strtree().

    Args:
        **kwargs: Mandatory and optional keyword arguments:
            source_features (sequence): The source features. Mandatory.
            source_geometries (sequence): The source geometries. Mandatory.
            source_idx: STRTree index for the source geometries. Mandatory.
            target_features (sequence): The target features. Mandatory.
            target_geometries (sequence): The target geometries. Mandatory.
            mask_features (sequence): The mask features. Mandatory.
            mask_geometries (sequence): The mask geometries. Mandatory.
            mask_idx: STRTree index for the mask geometries. Mandatory.
            attribute (str): The attribute (of the source features) to use.
                Mandatory.
            relative (bool): Boolean flag indicating if attribute should be
                considered relative. Defaults to False. Optional.
            ignore_pycnophylactic: Boolean flag indicating if the pycnophylactic
                property should be ignored. Defaults to False. Optional.

    Returns:
        list: Target features with their newly calculated attribute values.

    """
    logging.info("Running Binary Dasymetric Weighting...")

    source_features = kwargs["source_features"]
    source_geometries = kwargs["source_geometries"]
    target_features = kwargs["target_features"]
    target_geometries = kwargs["target_geometries"]
    mask_geometries = kwargs["mask_geometries"]
    mask_idx = kwargs["mask_idx"]
    attribute = kwargs["attribute"]
    relative = kwargs.get("relative")
    ignore_pycnophylactic = kwargs.get("ignore_pycnophylactic")

    # we must not mutate our source features but we mask their geometries
    logging.debug("Making a deepcopy of source_features...")
    source_features = deepcopy(source_features)

    logging.info("Masking source features...")
    for i, source in enumerate(tqdm(source_features)):
        to_dissolve = []
        source_geom = source_geometries[i]

        for hit in mask_idx.query(source_geom):
            mask_geom = mask_geometries[hit.i]

            intersection = mask_geom.intersection(source_geom)
            intersection = make_valid(intersection)

            if not intersection.is_empty and intersection.area != 0:
                to_dissolve.append(intersection)

        # Dissolve to get one masked source feature instead of multiple
        # single masked features referring to the prior source-feature.
        logging.info("Dissolving masked features per source...")
        new_geom = unary_union(to_dissolve)
        source["geometry"] = mapping(new_geom)

    logging.info("Generating masked source geometries...")
    source_masked_geometries = make_geometries(source_features)
    logging.info("Generating spatial index for masked source geometries...")
    source_masked_idx = generate_strtree(source_masked_geometries)

    logging.info("Processing target features...")
    for i, target in enumerate(tqdm(target_features)):
        target_geom = target_geometries[i]
        Pt = 0

        for hit in source_masked_idx.query(target_geom):
            source_feature = source_features[hit.i]
            source_value = source_feature["properties"][attribute]
            source_geom = source_masked_geometries[hit.i]

            intersection = source_geom.intersection(target_geom)
            intersection = make_valid(intersection)

            if not intersection.is_empty:

                Asc = source_geom.area
                Asct = intersection.area
                At = target_geom.area
                Ps = source_value

                if not relative:
                    Pt += (Asct / Asc) * Ps
                else:
                    Pt += (Asct / At) * Ps

        target["properties"][attribute] = Pt

    if not relative:
        check_pycnophylactic(
            source_features, target_features, attribute, ignore_pycnophylactic
        )

    return target_features


def nclass_dasymetric_weighting(**kwargs):
    """Perform n-class dasymetric weighting.

    The first step applies the mask weight per intersection with a source-
    feature and appends this parameter to that feature for all intersections
    between one source- and a mask-feature.

    Like with binary dasymetric weighting the source-features are then masked
    to reduce the area size to that of the mask while maintaining the sources'
    attribute values. Also within this step the weight calculation is applied
    to the masked source-features, where the weight of a mask-feature (Wsc) is
    related to the sum of all mask weights (sum_Wsc) for the respective
    source-feature and multiplied by the given source attribute value (Ps).
    This results in the weighted source attribute (Psc) within the masked
    source-feature.

    Like with simple area weighting for every target-feature the intersection
    area (Asct) is related to the intersecting masked source-feature area
    (Asc) (relative values: target-feature area (At)). The resulting quotient
    is multiplied by the weighted source-feature attribute value from the
    former step (Psc) and the result (Pt) is then summed up for all
    intersecting source-features per target feature.

    Any STRTree passed to this function must have its geometries tagged with their
    original index number, see generate_strtree().

    Args:
        **kwargs: Mandatory and optional keyword arguments:
            source_features (sequence): The source features. Mandatory.
            source_geometries (sequence): The source geometries. Mandatory.
            source_idx: STRTree index for the source geometries. Mandatory.
            target_features (sequence): The target features. Mandatory.
            target_geometries (sequence): The target geometries. Mandatory.
            mask_features (sequence): The mask features. Mandatory.
            mask_geometries (sequence): The mask geometries. Mandatory.
            mask_idx: STRTree index for the mask geometries. Mandatory.
            attribute (str): The attribute (of the source features) to use.
                Mandatory.
            weight (str): The weight attribute (of the target features) to use.
                Mandatory.
            relative (bool): Boolean flag indicating if attribute should be
                considered relative. Defaults to False. Optional.
            ignore_pycnophylactic: Boolean flag indicating if the pycnophylactic
                property should be ignored. Defaults to False. Optional.

    Returns:
        list: Target features with their newly calculated attribute values.

    """
    logging.info("Running N-Class Dasymetric Weighting...")

    source_features = kwargs["source_features"]
    source_geometries = kwargs["source_geometries"]
    target_features = kwargs["target_features"]
    target_geometries = kwargs["target_geometries"]
    mask_features = kwargs["mask_features"]
    mask_geometries = kwargs["mask_geometries"]
    mask_idx = kwargs["mask_idx"]
    attribute = kwargs["attribute"]
    weight = kwargs["weight"]
    relative = kwargs.get("relative")
    ignore_pycnophylactic = kwargs.get("ignore_pycnophylactic")

    #####
    # first we collect the weight values of all control zones that a source
    # feature intersects with
    logging.info("Calculating mask weight per intersection with source feature...")

    # we could just store the sum of Wsc values per source feature, but storing
    # all the separate values is closer to the paper and might help understanding
    all_Wsc_values = []  # same index as source_features

    for i, source in enumerate(tqdm(source_features)):
        source_geom = source_geometries[i]

        Wsc_values = []

        for hit in mask_idx.query(source_geom):
            mask_geom = mask_geometries[hit.i]

            intersection = mask_geom.intersection(source_geom)
            intersection = make_valid(intersection)

            if not intersection.is_empty and intersection.area != 0:
                mask_feature = mask_features[hit.i]
                mask_value = mask_feature["properties"][weight]

                Wsc_values.append(mask_value)
        all_Wsc_values.append(Wsc_values)

    #####
    # then we generate the smallest fragments of the overlay between source and
    # mask features and calculate the attribute value (Psc) for each of them
    logging.info(
        "Overlaying source and mask features & calculating weight per fragment..."
    )

    fragment_features = []

    for i, source in enumerate(tqdm(source_features)):
        source_geom = source_geometries[i]

        for hit in mask_idx.query(source_geom):
            mask_geom = mask_geometries[hit.i]

            intersection = mask_geom.intersection(source_geom)
            intersection = make_valid(intersection)

            if not intersection.is_empty and intersection.area != 0:
                mask_feature = mask_features[hit.i]
                mask_value = mask_feature["properties"][weight]

                fragment = {
                    "properties": {"Wsc": all_Wsc_values[i]},
                    "geometry": mapping(intersection),
                }

                Ps = source["properties"][attribute]
                Wc = mask_value
                sum_Wsc = sum(fragment["properties"]["Wsc"])

                if not relative:
                    fragment["properties"]["Psc"] = (Wc / sum_Wsc) * Ps
                else:
                    if Wc > 0:
                        fragment["properties"]["Psc"] = Ps
                    elif Wc == 0:
                        fragment["properties"]["Psc"] = 0

                fragment_features.append(fragment)

    logging.info("Generating masked source geometries...")
    fragment_geometries = make_geometries(fragment_features)
    logging.info("Generating spatial index for masked source geometries...")
    fragment_idx = generate_strtree(fragment_geometries)

    #####
    # finally a simply areal weighting is used to calculate the attribute values
    # for the target zones from the intersecting small fragments
    logging.info("Processing target features...")
    for i, target in enumerate(tqdm(target_features)):
        target_geom = target_geometries[i]

        Pt = 0

        for hit in fragment_idx.query(target_geom):
            fragment = fragment_features[hit.i]
            fragment_geom = fragment_geometries[hit.i]
            Psc = fragment["properties"]["Psc"]

            intersection = fragment_geom.intersection(target_geom)
            intersection = make_valid(intersection)

            if not intersection.is_empty and intersection.area != 0:

                Asct = intersection.area
                Asc = fragment_geom.area
                At = target_geom.area
                if not relative:
                    Pt += (Asct / Asc) * Psc
                else:
                    Pt += (Asct / At) * Psc

        target["properties"][attribute] = Pt

    if not relative:
        check_pycnophylactic(
            source_features, target_features, attribute, ignore_pycnophylactic
        )

    return target_features


def execute_cogran(function, **kwargs):
    """Run CoGran using provided file paths and attribute selections.

    This function returns an updated meta dictionary based on the original
    target schema but with a new field for the calculated attribute values.

    Args:
        function: CoGran function object
        **kwargs: Mandatory and optional keyword arguments:
            source (str): Path to Fiona-compatible file containing source
                features. Mandatory.
            target (str): Path to Fiona-compatible file containing target
                features. Mandatory.
            attribute (str): The attribute (of the source features) to use.
                Mandatory.
            mask (str): Path to Fiona-compatible file containing mask
                features. Mandatory if CoGran function requires it.
            weight (str): The attribute (of the mask features) to use.
                Mandatory if CoGran function requires it.
            relative (bool): Boolean flag indicating if attribute should be
                considered relative. Defaults to False. Optional.
            ignore_pycnophylactic (bool): Boolean flag indicating if the
                pycnophylactic property should be ignored. Defaults to False.
                Optional.

    Returns:
        target_features (sequence), target_meta (dict)

    Raises:
        CRSMismatchError: If source and target (and mask) data do not have the
            same coordinate reference system.
        CoGranMethodError: If the requirements of the selected CoGran function
            were not met.

    """
    #####
    # process arguments and initialise

    # mandatory arguments
    source = kwargs.get("source")
    target = kwargs.get("target")
    attribute = kwargs.get("attribute")

    # check mandatory arguments
    assert (
        source and target and attribute
    ), "source, target and attribute arguments must ALL be passed"

    # optional arguments that depend on the selected function
    # these are checked below with more context
    mask = kwargs.get("mask")
    weight = kwargs.get("weight")

    # truly optional arguments with defaults
    relative = kwargs.get("relative", False)
    ignore_pycnophylactic = kwargs.get("ignore_pycnophylactic", True)

    # initialising mask things to None to make sure we can use one function
    # parameter set below for all kinds of methods
    mask_features = mask_meta = mask_geometries = mask_idx = None

    #####
    # load, validate and prepare data

    # get features from files
    logging.info(f"Loading source features from {source}")
    source_features, source_meta = get_features(source)

    logging.info(f"Loading target features from {target}")
    target_features, target_meta = get_features(target)

    # make sure the CRS match, otherwise spatial relationships are nonsense
    if not source_meta["crs"] == target_meta["crs"]:
        raise CRSMismatchError("CRS of source and target do not match!")

    # check if the attribute field already exists in target,
    # log warning if so
    if attribute in target_meta["schema"]["properties"].keys():
        attribute_type = target_meta["schema"]["properties"][attribute]

        logging.warning(
            (
                f'Target schema already has a field "{attribute}"'
                f"({attribute_type}), this will be overwritten."
            )
        )

    # prepare geometries and index

    logging.info("Generating source geometries...")
    source_geometries = make_geometries(source_features)

    logging.info("Generating target geometries...")
    target_geometries = make_geometries(target_features)

    logging.info("Generating spatial index for source geometries...")
    source_idx = generate_strtree(source_geometries)

    #####
    # check if requirements of the selected CoGran function are met

    requirements = METHODS[function].get("requirements")

    # do we need a mask, did we get one?
    if requirements and "mask" in requirements and not mask:
        raise CoGranMethodError(f"{function} requires a MASK.")
    elif (not requirements or "mask" not in requirements) and mask:
        raise CoGranMethodError(f"MASK is not used for {function}.")
    elif requirements and "mask" in requirements and mask:
        # everything is fine
        mask_features, mask_meta = get_features(mask)

        if not source_meta["crs"] == target_meta["crs"] == mask_meta["crs"]:
            raise CRSMismatchError("CRS of source, target and mask do not match!")

        mask_geometries = make_geometries(mask_features)
        mask_idx = generate_strtree(mask_geometries)

    # do we need a weight, did we get one?
    if requirements and "weight" in requirements and not weight:
        raise CoGranMethodError(f"{function} requires a WEIGHT attribute.")
    elif (not requirements or "weight" not in requirements) and weight:
        raise CoGranMethodError(f"WEIGHT attribute is not used for {function}.")

    #####
    # finally run CoGran
    # holy kwargsamoly
    logging.info(f"Executing CoGran: {function}")
    target_features = function(
        source_features=source_features,
        source_geometries=source_geometries,
        source_idx=source_idx,
        target_features=target_features,
        target_geometries=target_geometries,
        mask_features=mask_features,
        mask_geometries=mask_geometries,
        mask_idx=mask_idx,
        attribute=attribute,
        weight=weight,
        relative=relative,
        ignore_pycnophylactic=ignore_pycnophylactic,
    )

    return target_features, target_meta  # FIXME return dict, not tuple


class Error(Exception):
    """Base-class for all exceptions raised by this module."""


class NotPycnophylacticError(Error):
    """Target did not get the same total sum of values as source."""


class CRSMismatchError(Error):
    """User supplied datasets with different CRS. This is fatal."""


class CoGranMethodError(Error):
    """Arguments did not match the selected CoGran function's requirements."""


# we can't move this to the top because the function objects are used as keys
METHODS = {
    areal_weighting: {"name": "Areal Weighting", "snake": "areal_weighting"},
    attribute_weighting: {
        "name": "Attribute Weighting",
        "snake": "attribute_weighting",
        "requirements": ["weight"],
    },
    binary_dasymetric_weighting: {
        "name": "Binary Dasymetric Weighting",
        "snake": "binary_dasymetric_weighting",
        "requirements": ["mask"],
    },
    nclass_dasymetric_weighting: {
        "name": "N-Class Dasymetric Weighting",
        "snake": "nclass_dasymetric_weighting",
        "requirements": ["mask", "weight"],
    },
}
