#!/usr/bin/env python3
import inspect
import logging
import os
import pkg_resources  # to get the version number
import sys

import fiona  # to extract the properties/attributes

from PyQt5.QtCore import pyqtSlot, QSettings, Qt
from PyQt5.QtWidgets import (
    QPushButton,
    QHBoxLayout,
    QVBoxLayout,
    QTextBrowser,
    QDialog,
    QTextEdit,
    QLabel,
    QApplication,
    QGridLayout,
    QCheckBox,
    QProgressBar,
    QComboBox,
    QFileDialog,
    QMessageBox,
)
from PyQt5 import QtGui
from qtmodern import styles  # Dark scheme


import cogran
from cogran.helpers import get_numeric_attributes, to_file


class QTextBrowserLogger(logging.Handler):
    """A QTextBrowser based logging handler."""

    # via https://stackoverflow.com/questions/28655198/best-way-to-display-logs-in-pyqt/
    # 51641943#51641943
    def __init__(self, parent):
        """Initialise, add .widget with the actual QTextBrowser.

        Args:
            parent: The GUI class we want to add this to.

        """
        super().__init__()
        self.widget = QTextBrowser(parent)
        self.widget.setReadOnly(True)

    def emit(self, record):
        """On new logging message, update the QTextBrowser.

        Args:
            record: The logging message.

        """
        msg = self.format(record)
        self.widget.append(msg)

    def append(self, msg):
        """Append text to the QTextBrowser.

        Convenience function so we don't have to use .widget everywhere.

        Args:
            msg: The message to append.

        """
        self.widget.append(msg)


class CogranUI(QDialog):
    def __init__(self):
        super().__init__()  # init according to QDialog

        #####
        # Investigate available Fiona drivers and prepare filters for File Open dialogs

        # neither fiona nor ogr have a nice mapping of driver->extension(s)
        # so we must manually define them here...
        known_drivers = {
            "ESRI Shapefile": ["*.shp"],
            "GeoJSON": ["*.json", "*.geojson"],
            "GPKG": ["*.gpkg"],
            "GML": ["*.gml", "*.xml"],
        }

        # which drivers are available and which
        # modes they support depends on the user's fiona
        supported_drivers = {
            d: modes
            for (d, modes) in fiona.supported_drivers.items()
            if known_drivers.get(d)
        }
        logging.debug(f"fiona.supported_drivers: {supported_drivers}")

        # for the readers we need to collect all the driver's extensions
        fiona_readers = [  # noqa
            d for (d, modes) in supported_drivers.items() if "r" in modes
        ]

        # the user might need to know the supported extensions so we store them
        self.readers_extensions = [
            ext for exts in known_drivers.values() for ext in exts
        ]  # flattened

        self.readers_filter = (
            f"Geodata ({' '.join(self.readers_extensions)});;Any (*.*)"
        )

        # for the writers the user chooses their own extension, we just need the drivers
        fiona_writers = [  # noqa
            d for (d, modes) in supported_drivers.items() if "w" in modes
        ]
        filters = [f"{d} ({' '.join(known_drivers[d])})" for d in supported_drivers]

        self.writers_filter = ";;".join(filters)

        #####
        # Prepare the cogran methods' docstrings for fancy display

        # Get the Method Docstrings from CoGran
        self.cgdoc = {}
        for method, meta in cogran.METHODS.items():
            method_doc = inspect.getdoc(method)
            method_doc = method_doc[0 : method_doc.find("Args:")]
            method_name = cogran.METHODS[method]["name"]
            self.cgdoc[method_name] = QtGui.QTextDocument(
                method_doc
            )  # the font is fixed later

            #####
            # FIXME removed until we figure out packaging the docs/ directory

            # Get application path for relative imports
            # if getattr(sys, "frozen", False):
            #     # If the application is run as a bundle, the pyInstaller bootloader
            #     # extends the sys module by a flag frozen=True and sets the app path
            #       into variable _MEIPASS'.
            #     ap = sys._MEIPASS
            # else:
            #     ap = os.path.dirname(os.path.abspath(__file__))
            #
            # method_image = QtGui.QImage(os.path.join(
            # ap, f"../docs/images/{method}.png"), "PNG"
            # )
            # cursor = QtGui.QTextCursor(self.cgdoc[method])
            # cursor.movePosition(QtGui.QTextCursor.End, QtGui.QTextCursor.MoveAnchor)
            # cursor.insertText("\n\n")
            # cursor.insertImage(method_image)

        #####
        # Initialise the GUI

        self.setWindowTitle("CoGran")

        # for scaling text fields later
        self.fontsize = self.font().pointSize()
        self.padding_factor = 3

        # restore saved GUI settings if available
        self.settings = QSettings("g2lab, HafenCity Universität Hamburg", "CoGran")
        self.restoreGeometry(self.settings.value("geometry", bytes()))

        # Initialise cogran attributes
        # This way all instance variables relevant to cogran are nicely listed
        # in one place and the code below can always assume they exist
        self.method = None
        self.source = None
        self.target = None
        self.attribute = None
        self.mask = None
        self.weight = None
        self.relative = None
        self.ignore_pycnophylactic = None
        self.output = None

        # Set vertical main layout
        self.vbox = QVBoxLayout()
        self.setLayout(self.vbox)

        # Add help button
        self.buttonHelp = QPushButton("Help", self)
        self.hboxHelp = QHBoxLayout()
        self.hboxHelp.addStretch(1)
        self.hboxHelp.addWidget(self.buttonHelp)
        self.vbox.addLayout(self.hboxHelp)
        self.helpWindow = HelpWindow()

        # Add feedback browser, attach logging
        self.textBrowserFeedback = QTextBrowserLogger(self)
        self.textBrowserFeedback.widget.setText("Welcome to CoGran!")
        self.textBrowserFeedback.widget.setMaximumHeight(80)

        self.textBrowserFeedback.setFormatter(
            logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        )
        logging.getLogger().addHandler(self.textBrowserFeedback)
        logging.getLogger().setLevel(logging.INFO)

        self.hboxBrowserFeedback = QHBoxLayout()
        self.hboxBrowserFeedback.addWidget(self.textBrowserFeedback.widget)
        self.vbox.addLayout(self.hboxBrowserFeedback)

        # Add gridlayout for the main stuff(tm)
        self.gridLayoutMain = QGridLayout()
        self.vbox.addLayout(self.gridLayoutMain)

        # Add HBox for the checkboxes
        self.hboxOptions = QHBoxLayout()
        self.hboxOptions.addStretch(1)
        self.vbox.addLayout(self.hboxOptions)

        # Add checkbox for relative values
        self.checkBoxRelative = QCheckBox()
        self.checkBoxRelative.setText("Relative Values?")
        self.hboxCheckRelative = QHBoxLayout()
        self.hboxCheckRelative.addWidget(self.checkBoxRelative)
        self.hboxOptions.addWidget(self.checkBoxRelative)

        # Add checkbox for assuring the pycnophylactic property
        self.checkBoxPycno = QCheckBox()
        self.checkBoxPycno.setText("Assure Pycnophylactic Property?")
        self.checkBoxPycno.setChecked(True)
        self.hboxCheckPycno = QHBoxLayout()
        self.hboxCheckPycno.addWidget(self.checkBoxPycno)
        self.hboxOptions.addWidget(self.checkBoxPycno)

        self.hboxOptions.addStretch(100)  # stretcher item to push items left

        # Add progress bar
        self.progressBar = QProgressBar()
        self.progressBar.setRange(0, 1)
        self.vbox.addWidget(self.progressBar)
        self.progressBar.setEnabled(False)

        # Add combo box for method
        self.comboMethod = QComboBox()
        self.comboMethod.addItems([m["name"] for f, m in cogran.METHODS.items()])

        # Add combo box for attribute, hidden by default
        self.comboAttribute = QComboBox()
        self.comboAttribute.addItem("Attribute")
        self.comboAttribute.model().item(0).setEnabled(False)

        # Add combo box for weight, hidden by default
        self.comboWeight = QComboBox()
        self.comboWeight.addItem("Weight")
        self.comboWeight.model().item(0).setEnabled(False)

        # Add buttons and text fields
        self.buttonSource = QPushButton("Source File...", self)
        self.buttonTarget = QPushButton("Target File...", self)
        self.buttonMask = QPushButton("Mask File...", self)
        self.buttonOutput = QPushButton("Output File...", self)

        self.textMethodInfo = QTextEdit()
        self.textMethodInfo.setReadOnly(True)

        self.textEditSource = QTextEdit()
        self.textEditSource.setReadOnly(True)
        self.textEditSource.setMaximumHeight(self.fontsize * self.padding_factor)
        self.textEditSource.setEnabled(False)

        self.textEditTarget = QTextEdit()
        self.textEditTarget.setReadOnly(True)
        self.textEditTarget.setMaximumHeight(self.fontsize * self.padding_factor)
        self.textEditTarget.setEnabled(False)

        self.textEditAttribute = QTextEdit()
        self.textEditAttribute.setReadOnly(True)
        self.textEditAttribute.setMaximumHeight(self.fontsize * self.padding_factor)
        self.textEditAttribute.setEnabled(False)

        self.textEditMask = QTextEdit()
        self.textEditMask.setReadOnly(True)
        self.textEditMask.setMaximumHeight(self.fontsize * self.padding_factor)
        self.textEditMask.setEnabled(False)

        self.textEditWeight = QTextEdit()
        self.textEditWeight.setReadOnly(True)
        self.textEditWeight.setMaximumHeight(self.fontsize * self.padding_factor)
        self.textEditWeight.setEnabled(False)

        self.textEditOutput = QTextEdit()
        self.textEditOutput.setReadOnly(True)
        self.textEditOutput.setMaximumHeight(self.fontsize * self.padding_factor)
        self.textEditOutput.setEnabled(False)

        # Define the layout positions for the combo method and buttons
        self.gridLayoutMain.addWidget(self.comboMethod, 0, 0)
        self.gridLayoutMain.addWidget(self.buttonSource, 1, 0)
        self.gridLayoutMain.addWidget(self.buttonTarget, 2, 0)
        self.gridLayoutMain.addWidget(self.comboAttribute, 3, 0)
        self.gridLayoutMain.addWidget(self.buttonMask, 4, 0)
        self.gridLayoutMain.addWidget(self.comboWeight, 5, 0)
        self.gridLayoutMain.addWidget(self.buttonOutput, 6, 0)

        # Define the layout positions for other elements
        self.gridLayoutMain.addWidget(self.textMethodInfo, 0, 1)
        self.gridLayoutMain.addWidget(self.textEditSource, 1, 1)
        self.gridLayoutMain.addWidget(self.textEditTarget, 2, 1)
        self.gridLayoutMain.addWidget(self.textEditAttribute, 3, 1)
        self.gridLayoutMain.addWidget(self.textEditMask, 4, 1)
        self.gridLayoutMain.addWidget(self.textEditWeight, 5, 1)
        self.gridLayoutMain.addWidget(self.textEditOutput, 6, 1)

        # Define run button
        self.buttonRun = QPushButton()
        self.buttonRun.setText("Run")
        self.RunLayout = QHBoxLayout()
        self.RunLayout.addWidget(self.buttonRun)
        self.RunLayout.addStretch(1)
        self.vbox.addLayout(self.RunLayout)

        # Define exit button and version label
        self.buttonExit = QPushButton()
        self.buttonExit.setText("Exit")
        self.textVersion = QLabel()
        cogran_version = pkg_resources.require("cogran")[0].version
        self.textVersion.setText(f"CoGran Version: {cogran_version}")
        self.ExitLayout = QHBoxLayout()
        self.ExitLayout.addWidget(self.textVersion)
        self.ExitLayout.addStretch(1)
        self.ExitLayout.addWidget(self.buttonExit)
        self.vbox.addLayout(self.ExitLayout)

        # Set connections between gui elements and methods
        self.buttonHelp.clicked.connect(self.helpWindow.show)
        self.comboMethod.currentIndexChanged.connect(self.combo_method_activated)
        self.comboAttribute.currentIndexChanged.connect(self.combo_attribute_activated)
        self.comboWeight.currentIndexChanged.connect(self.combo_weight_activated)
        self.buttonSource.clicked.connect(self.on_button_source_clicked)
        self.buttonTarget.clicked.connect(self.on_button_target_clicked)
        self.buttonMask.clicked.connect(self.on_button_mask_clicked)
        self.buttonOutput.clicked.connect(self.on_button_output_clicked)
        self.buttonRun.clicked.connect(self.on_button_run_clicked)
        self.buttonExit.clicked.connect(self.on_button_exit_clicked)
        self.checkBoxRelative.stateChanged.connect(self.on_checkbox_relative_clicked)
        self.checkBoxPycno.stateChanged.connect(self.on_checkbox_pycno_clicked)

        # Disable all buttons and text fields by default
        for widget in [
            self.buttonMask,
            self.comboAttribute,
            self.comboWeight,
            self.textEditAttribute,
            self.textEditMask,
            self.textEditOutput,
            self.textEditSource,
            self.textEditTarget,
            self.textEditWeight,
        ]:
            widget.setEnabled(False)

        # Finalize
        # Run Method to disable all buttons and hide mask and weight
        self.combo_method_activated()

    # Combo Box with very ugly enabling of Buttons
    @pyqtSlot()
    def combo_method_activated(self):
        """Runs when a method was selected or the selection was changed.

        This gets run once on program launch!"""

        logging.debug("combo_method_activated")

        # Set Method Attribute
        self.method = self.comboMethod.currentText()
        self.progressBar.setValue(0)

        # Enable buttons depending on the method

        # Add Method Description
        # We need to change the font from some ugly default
        # to the main font our GUI uses. This is done here
        # because the methods' texts were collected outside.
        description = self.cgdoc[self.method]
        description.setDefaultFont(self.font())
        self.textMethodInfo.setDocument(description)

        # always enable things that are always used
        for widget in [
            self.buttonSource,
            self.buttonTarget,
            self.buttonOutput,
            self.checkBoxRelative,
            self.checkBoxPycno,
        ]:
            widget.setEnabled(True)

        # if these are available from a previous run, "restore" them again

        if self.source:
            self.textEditSource.setEnabled(True)

        if self.target:
            self.textEditTarget.setEnabled(True)

        if self.output:
            self.textEditOutput.setEnabled(True)

        if self.attribute:
            self.textEditAttribute.setEnabled(True)
            self.comboAttribute.setEnabled(True)

        if self.method in (
            "Binary Dasymetric Weighting",
            "N-Class Dasymetric Weighting",
        ):
            self.buttonMask.setEnabled(True)
            # restore mask if set before already
            if self.textEditMask.toPlainText():
                self.textEditMask.setEnabled(True)
        else:
            self.buttonMask.setEnabled(False)
            self.textEditMask.setEnabled(False)

        if (self.method == "Attribute Weighting" and self.target) or (
            self.method == "N-Class Dasymetric Weighting" and self.mask
        ):
            self.comboWeight.setEnabled(True)
            # restore weight if set before already
            if self.weight:
                self.textEditWeight.setEnabled(True)
        else:
            self.comboWeight.setEnabled(False)
            self.textEditWeight.setEnabled(False)

        self.check_run()

    @pyqtSlot()
    def combo_attribute_activated(self):
        """Runs when an attribute was selected or the attribute selection was changed.
        """

        logging.debug("combo_attribute_activated")

        if self.comboAttribute.currentText() == "Attribute":
            self.textEditAttribute.clear()
            self.textEditAttribute.setEnabled(False)
            self.attribute = None
            self.textBrowserFeedback.append("Please select an attribute: ")
        else:
            attribute = self.comboAttribute.currentText()
            if attribute:
                self.attribute = attribute
                self.textEditAttribute.setText(self.attribute)
                self.textEditAttribute.setEnabled(True)
                self.textBrowserFeedback.append(
                    f"Attribute '{self.attribute}' selected."
                )

        self.check_run()

    @pyqtSlot()
    def combo_weight_activated(self):
        """Runs when a weight attribute was selected or the weight attribute selection
        was changed."""

        logging.debug("combo_weight_activated")

        if self.comboWeight.currentText() == "Weight":
            self.weight = None
            self.textEditWeight.clear()
            self.textEditWeight.setEnabled(False)
            self.textBrowserFeedback.append("Please select a weight attribute: ")
        else:
            weight = self.comboWeight.currentText()
            if weight:
                self.weight = weight
                self.textEditWeight.setText(self.weight)
                self.textEditWeight.setEnabled(True)
                self.textBrowserFeedback.append(
                    f"Weight attribute '{self.weight}' selected."
                )

        self.check_run()

    @pyqtSlot()
    def on_button_source_clicked(self):

        logging.debug("on_button_source_clicked")

        path, _ = QFileDialog.getOpenFileUrl(
            self, "Select your source file: ", filter=self.readers_filter
        )

        filepath = path.toLocalFile()
        try:
            # try to open it with fiona to make sure it is valid
            with fiona.open(filepath, "r") as source:  # noqa
                self.source = filepath
                self.textBrowserFeedback.append(
                    f"Source file '{self.source}' selected."
                )
                self.textEditSource.setEnabled(True)
                self.textEditSource.setText(filepath)
                self.refresh_attributes()
        except fiona.errors.DriverError as e:
            logging.debug(e)
            self.textBrowserFeedback.append(
                f"The selected source file '{self.source}' is not valid."
                f"The file format must be one of {', '.join(self.readers_extensions)}"
                "Please provide a single source file with a valid path."
            )
            self.textEditSource.clear()
            self.textEditSource.setEnabled(False)
            self.source = None

        self.check_run()

    @pyqtSlot()
    def on_button_target_clicked(self):

        logging.debug("on_button_target_clicked")

        path, _ = QFileDialog.getOpenFileUrl(
            self, "Select your target file: ", filter=self.readers_filter
        )

        filepath = path.toLocalFile()
        try:
            # try to open it with fiona to make sure it is valid
            with fiona.open(filepath, "r") as target:  # noqa
                self.target = filepath
                self.textBrowserFeedback.append(
                    f"Target file '{self.target}' selected."
                )
                self.textEditTarget.setEnabled(True)
                self.textEditTarget.setText(filepath)

                if self.method == "Attribute Weighting":
                    self.refresh_weight(self.target)
        except fiona.errors.DriverError as e:
            logging.debug(e)
            self.textBrowserFeedback.append(
                f"The selected target file '{self.target}' is not valid."
                f"The file format must be one of {', '.join(self.readers_extensions)}"
                "Please provide a single source file with a valid path."
            )
            self.textEditTarget.clear()
            self.textEditTarget.setEnabled(False)
            self.target = None

        self.check_run()

    @pyqtSlot()
    def on_button_mask_clicked(self):

        logging.debug("on_button_mask_clicked")

        path, _ = QFileDialog.getOpenFileUrl(
            self, "Select your mask file: ", filter=self.readers_filter
        )

        filepath = path.toLocalFile()
        try:
            # try to open it with fiona to make sure it is valid
            with fiona.open(filepath, "r") as mask:  # noqa
                self.mask = filepath
                self.textBrowserFeedback.append(f"Mask file '{self.mask}' selected.")
                self.textEditMask.setEnabled(True)
                self.textEditMask.setText(filepath)
                self.refresh_weight(self.mask)
        except fiona.errors.DriverError as e:
            logging.debug(e)
            self.textBrowserFeedback.append(
                f"The selected mask file '{self.mask}' is not valid."
                f"The file format must be one of {', '.join(self.readers_extensions)}"
                "Please provide a single source file with a valid path."
            )
            self.textEditMask.clear()
            self.textEditMask.setEnabled(False)
            self.mask = None

        self.check_run()

    @pyqtSlot()
    def on_button_output_clicked(self):

        logging.debug("on_button_output_clicked")

        path, filter = QFileDialog.getSaveFileUrl(
            self, "Save File", filter=self.writers_filter
        )

        filepath = path.toLocalFile()
        # watch out, this is structured the other way around from the other _clicked
        # functions! TODO is it still?
        if (
            path.isValid()
            and os.access(os.path.dirname(filepath), os.W_OK)
            or (os.path.isfile(filepath) and os.access(filepath, os.W_OK))
        ):
            logging.debug(f"Output format: {filter}")

            # filter is something like "ESRI Shapefile (*.shp)"
            # we just need the path before the " (...)" so:
            extension_index = filter.index("(") - 1
            self.output_driver = filter[:extension_index]

            self.output = filepath
            self.textBrowserFeedback.append(
                f"Output will be written to '{self.output}' using {self.output_driver}"
            )
            self.textEditOutput.setEnabled(True)
            self.textEditOutput.setText(filepath)
        else:
            self.textBrowserFeedback.append(
                f"The selected output '{self.output}' is not valid."
            )
            self.textEditOutput.setEnabled(False)
            self.output = None

        self.check_run()

    @pyqtSlot()
    def on_checkbox_relative_clicked(self):
        logging.debug("on_checkbox_relative_clicked")
        if self.checkBoxRelative.isChecked():
            self.relative = True
        else:
            self.relative = False
        logging.debug(f"self.relative is now: {self.relative}")

    @pyqtSlot()
    def on_checkbox_pycno_clicked(self):
        logging.debug("on_checkbox_pycno_clicked")
        if self.checkBoxPycno.isChecked():
            self.ignore_pycnophylactic = False
        else:
            self.ignore_pycnophylactic = True
        logging.debug(
            f"self.ignore_pycnophylactic is now: {self.ignore_pycnophylactic}"
        )

    @pyqtSlot()
    def on_button_run_clicked(self):

        logging.debug("on_button_run_clicked")

        # Set Up
        self.textBrowserFeedback.append("Running CoGran...")
        self.progressBar.setRange(
            0, 0
        )  # if both are set to 0, progress bar is showing busy indicator
        self.progressBar.setEnabled(True)

        # get the actual function object for the function name the user selected
        function = [
            function
            for function, meta in cogran.METHODS.items()
            if meta["name"] == self.method
        ][0]

        target_features, target_meta = cogran.execute_cogran(
            function,
            source=self.source,
            target=self.target,
            mask=self.mask,
            attribute=self.attribute,
            weight=self.weight,
            relative=self.relative,
            ignore_pycnophylactic=self.ignore_pycnophylactic,
        )

        to_file(
            target_features,
            target_meta,
            self.attribute,
            self.output,
            self.output_driver,
        )

        # Tear Down
        self.progressBar.setRange(0, 1)
        self.progressBar.setValue(1)
        self.textBrowserFeedback.append("CoGran has finished successfully!")

    @pyqtSlot()
    def on_button_exit_clicked(self):
        """Runs when user clicks Exit button.

        We handle saving window geometry in closeEvent so we want that used
        when the user hits the Exit button."""
        logging.debug("on_button_exit_clicked")
        self.close()

    @pyqtSlot()
    def keyPressEvent(self, event):
        """Override default keyPressEvent to handle Esc.

        We handle saving window geometry in closeEvent so we want that used
        when the user hits the Esc key.
        """
        if event.key() == Qt.Key_Escape:
            logging.debug("Qt.Key_Escape")
            self.close()
        else:
            # otherwise propagate event to parent (not overridden) keyPressEvent
            super().keyPressEvent(event)

    @pyqtSlot()
    def closeEvent(self, event):
        """Runs on self.close(). E.g. when the window's X button is clicked.

        The name is predefined by the API."""
        logging.debug("closeEvent")

        choice = QMessageBox.question(
            self,
            "Closing!",
            "Do you want to exit CoGran?",
            QMessageBox.Yes | QMessageBox.No,
        )

        if choice == QMessageBox.Yes:
            self.settings.setValue("geometry", self.saveGeometry())
            event.accept()
        else:
            event.ignore()

    def refresh_attributes(self):

        logging.debug("refresh_attributes")

        self.comboAttribute.clear()
        self.comboAttribute.addItem("Attribute")
        self.comboAttribute.model().item(0).setEnabled(False)

        attr = get_numeric_attributes(self.source)
        if attr:
            self.comboAttribute.addItems(attr)
            self.comboAttribute.setEnabled(True)
        else:
            self.attribute = None
            self.textBrowserFeedback.append(
                f"'{self.source}' does not contain any attributes"
            )
            self.textEditAttribute.clear()
            self.textEditAttribute.setEnabled(False)
            self.comboAttribute.setEnabled(False)
            self.source = None

    def refresh_weight(self, layer):

        logging.debug("refresh_weight")

        self.comboWeight.clear()
        self.comboWeight.addItem("Weight")
        self.comboWeight.model().item(0).setEnabled(False)

        attr = get_numeric_attributes(layer)
        if attr:
            self.comboWeight.addItems(attr)
            self.comboWeight.setEnabled(True)
        else:
            self.weight = None
            self.textBrowserFeedback.append(
                f"'{layer}' does not contain any attributes for weighting"
            )
            self.textEditWeight.clear()
            self.textEditWeight.setEnabled(False)
            self.comboWeight.setEnabled(False)
            layer = None

    def check_run(self):
        """
        Checks if Cogran is ready to Run.
        If a variable is missing it prints a Feedback to the user and displays the
        missing variable.
        Finally it enables the run button so the user can start cogran
        """

        logging.debug("check_run")

        self.buttonRun.setEnabled(False)

        if not self.source:
            self.textBrowserFeedback.append("Please select a source file: ")
        elif not self.target:
            self.textBrowserFeedback.append("Please select a target file: ")
        elif not self.output:
            self.textBrowserFeedback.append("Please specify an output file: ")
        elif not self.attribute:
            self.textBrowserFeedback.append("Please select the attribute: ")
        elif not self.weight and self.method in (
            "Attribute Weighting",
            "N-Class Dasymetric Weighting",
        ):
            self.textBrowserFeedback.append("Please select a weight attribute: ")
        elif not self.mask and self.method in (
            "Binary Dasymetric Weighting",
            "N-Class Dasymetric Weighting",
        ):
            self.textBrowserFeedback.append("Please select a mask file: ")
        else:
            self.buttonRun.setEnabled(True)
            self.textBrowserFeedback.append("CoGran is ready to run")


class HelpWindow(QDialog):
    def __init__(self):
        super().__init__()

        # Window Attributes
        self.setWindowTitle("CoGran Help")
        self.setGeometry(40, 40, 1600, 800)

        # Create Layout
        self.mainLayout = QVBoxLayout()
        self.setLayout(self.mainLayout)

        # Add Help Widget
        self.textHelp = QTextBrowser()
        self.textHelp.resize(800, 800)
        self.textHelp.setOpenExternalLinks(True)
        self.mainLayout.addWidget(self.textHelp)

        # Add Method Description
        # FIXME removed until we figure out packaging the docs/ directory
        # with open(os.path.join(ap, '../docs/help.html')) as f:
        #    self.textHelp.setHtml(f.read())
        self.textHelp.setHtml(
            (
                "<a href='https://gitlab.com/g2lab/cogran-python/issues/38'>"
                "Not bundled into the package yet.</a>"
            )
        )


def main():
    app = QApplication(sys.argv)  # Create Application Object
    window = CogranUI()  # Create base class of user interface object

    styles.dark(app)  # Dark Scheme from qtmodern
    window.show()
    sys.exit(app.exec_())  # Enter the mainloop of the application, exit when done


# Start application
if __name__ == "__main__":
    main()
