#!/usr/bin/env python3
"""A command line interface for cogran."""
import logging
import click

import cogran
from cogran.helpers import to_file

logging.basicConfig(
    format="%(asctime)s.%(msecs)03d\t%(levelname)s\t%(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.INFO,
)

# can't believe this is not built-in in any way...
logging_levels = "CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"

# get snake-case method names from cogran for CLI usage
methods = [meta["snake"] for function, meta in cogran.METHODS.items()]


@click.command()
@click.option(
    "--source", type=click.Path(exists=True), required=True, help="Source file."
)
@click.option(
    "--target", type=click.Path(exists=True), required=True, help="Target file."
)
@click.option("--mask", type=click.Path(exists=True), help="Mask file.")
@click.option(
    "--attribute",
    type=click.STRING,
    required=True,
    help="The attribute (of the source features) to use.",
)
@click.option(
    "--method", type=click.Choice(methods), required=True, help="Method to use."
)
@click.option(
    "--relative", type=click.BOOL, help="Treat attribute values as relative values."
)
@click.option(
    "--ignore_pycnophylactic",
    type=click.BOOL,
    help="Ignore the pycnophylactic property.",
)
@click.option(
    "--weight",
    type=click.STRING,
    help="The weight attribute (of the target or mask features) to use.",
)
@click.option(
    "--output", type=click.Path(), required=True, help="Output file NAME/PATH."
)
@click.option(
    "--driver",
    type=click.STRING,
    required=True,
    help="File format/driver to use for output.",
)
@click.option(
    "--verbosity", type=click.Choice(logging_levels), help="The level of verbosity."
)
def main(**kwargs):
    """Run CoGran.

    Args:
        **kwargs: Mandatory and optional keyword arguments:
            method (str): The spatial method to use.
            source (str): Path to Fiona-compatible file containing source
                features. Mandatory.
            target (str): Path to Fiona-compatible file containing target
                features. Mandatory.
            attribute (str): The attribute (of the source features) to use.
                Mandatory.
            mask (str): Path to Fiona-compatible file containing mask
                features. Mandatory if CoGran function requires it.
            weight (str): The attribute (of the mask features) to use.
                Mandatory if CoGran function requires it.
            relative (bool): Boolean flag indicating if attribute should be
                considered relative. Defaults to False. Optional.
            ignore_pycnophylactic (bool): Boolean flag indicating if the
                pycnophylactic property should be ignored. Defaults to False.
                Optional.
            output (str): File path where the output features will be written to.
            driver (str): The Fiona/GDAL driver to use for the output.
            verbosity (str): The desired level of verbosity.

    """
    verbosity = kwargs.get("verbosity")
    if verbosity:
        logger = logging.getLogger()
        level = logging.getLevelName(verbosity)
        logger.setLevel(level)

    # get the actual function object for the function name the user selected
    function = [
        function
        for function, meta in cogran.METHODS.items()
        if meta["snake"] == kwargs["method"]
    ][0]

    target_features, target_meta = cogran.execute_cogran(function, **kwargs)

    to_file(
        target_features,
        target_meta,
        kwargs["attribute"],
        kwargs["output"],
        kwargs["driver"],
    )


if __name__ == "__main__":
    main()
