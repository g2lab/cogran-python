import logging
import unittest
from testfixtures import LogCapture
from cogran import is_pycnophylactic


class TestCoGran_is_pycnophylactic(unittest.TestCase):
    def setUp(self):
        self.attribute = "attribute"

        # we don't need full features, just properties->attribute
        # is enough. surely we _should_ use proper features though...?

        self.source_features = [
            {"properties": {"attribute": 2}},
            {"properties": {"attribute": 3}},
        ]

        self.target_features = [
            {"properties": {"attribute": 1}},
            {"properties": {"attribute": 4}},
        ]

        self.target_features_not_p = [
            {"properties": {"attribute": 2}},
            {"properties": {"attribute": 2.9}},
        ]

    def test_is_pycnophylactic_5eq5(self):
        result = is_pycnophylactic(
            self.source_features, self.target_features, self.attribute
        )
        self.assertTrue(result)

    def test_is_pycnophylactic_5ne49(self):
        result = is_pycnophylactic(
            self.source_features, self.target_features_not_p, self.attribute
        )
        self.assertFalse(result)

    def test_is_pycnophylactic_5eq49reltol2(self):
        """is 5 compared to 4.9 with 2% tolerance pycnophylactic?"""

        result = is_pycnophylactic(
            self.source_features,
            self.target_features_not_p,
            self.attribute,
            rel_tol=0.02,
        )
        self.assertTrue(result)

    def test_is_pycnophylactic_logging_ok(self):
        with LogCapture() as log:
            logger = logging.getLogger()
            logger.setLevel(logging.INFO)

            is_pycnophylactic(
                self.source_features,
                self.target_features,
                self.attribute,
                rel_tol=0.001,
            )

            log.check(
                (
                    "root",
                    "INFO",
                    "Pycnophylactic property: 5 (source)"
                    " is within 0.001% of 5 (target).",
                )
            )

    def test_is_pycnophylactic_logging_notok(self):
        with LogCapture() as log:
            logger = logging.getLogger()
            logger.setLevel(logging.INFO)

            is_pycnophylactic(
                self.source_features,
                self.target_features_not_p,
                self.attribute,
                rel_tol=0.019,
            )

            log.check(
                (
                    "root",
                    "INFO",
                    "Pycnophylactic property: 5 (source)"
                    " is NOT within 0.019% of 4.9 (target).",
                )
            )


if __name__ == "__main__":
    unittest.main()
