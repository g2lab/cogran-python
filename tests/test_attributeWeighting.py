import os
import sys
import unittest
import cogran

# insert the directory in path
sys.path.insert(0, "../")
sys.path.append(os.getcwd())


class TestCoGranAttributeWeightingAbsoluteValues(unittest.TestCase):
    def setUp(self):
        self.target_features, _ = cogran.execute_cogran(
            source="tests/attributeWeighting/attributeWeighting_source.geojson",
            target="tests/attributeWeighting/attributeWeighting_target.geojson",
            attribute="Ps",
            weight="Qt",
            function=cogran.attribute_weighting,
        )

    def test_AttributeWeighting_target_feature_count_absolute_values(self):
        self.assertEqual(len(self.target_features), 3)

    def test_AttributeWeighting_target_feature_Pt_absolute_values(self):
        # TODO comment why places=2
        self.assertAlmostEqual(
            self.target_features[0]["properties"]["Ps"], 1.80, places=2
        )
        self.assertAlmostEqual(
            self.target_features[1]["properties"]["Ps"], 3.47, places=2
        )
        self.assertAlmostEqual(
            self.target_features[2]["properties"]["Ps"], 1.73, places=2
        )


if __name__ == "__main__":
    unittest.main()
