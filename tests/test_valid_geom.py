import unittest
from cogran.helpers import make_valid
from shapely.geometry import Point, LinearRing, Polygon


class TestValidGeom(unittest.TestCase):
    def test_skip_valid_geom(self):
        valid_polygon = Polygon(((0, 0), (1, 0), (1, 1), (0, 1), (0, 0)))
        self.assertIs(make_valid(valid_polygon), valid_polygon)

    def test_skip_valid_point_geometries(self):
        p = Point(0, 0)
        self.assertIs(make_valid(p), p)

    def test_skip_invalid_line_geometries(self):
        lr = LinearRing(((0, 0), (1, 2), (2, 0), (0, 1)))
        self.assertFalse(lr.is_valid)
        self.assertIs(make_valid(lr), lr)

    def test_fix_bowtie(self):
        bowtie = Polygon(((0, 0), (0, 2), (2, 0), (2, 2), (0, 0)))
        fixed_bowtie = Polygon(
            ((0, 0), (0, 2), (1, 1), (0, 0))
        )  # TODO WTF this is just half of it... # FIXME?
        self.assertEqual(make_valid(bowtie), fixed_bowtie)


if __name__ == "__main__":
    unittest.main()
