import os
import sys
import unittest
import cogran

# Insert the directory in first place of path
sys.path.insert(0, "../")
sys.path.append(os.getcwd())


class TestCoGranArealWeighting(unittest.TestCase):
    def test_ArealWeighting_target_feature_Pt_absolute_values(self):
        """4 square features with value 2 each get halved."""
        target_features, _ = cogran.execute_cogran(
            source="tests/arealWeighting/arealWeighting_source.geojson",
            target="tests/arealWeighting/arealWeighting_target.geojson",
            attribute="abs_val",
            function=cogran.areal_weighting,
        )

        for f in target_features:
            val = f["properties"]["abs_val"]
            self.assertEqual(val, 1)

    def test_ArealWeighting_target_feature_Pt_relative_values(self):
        target_features, _ = cogran.execute_cogran(
            source="tests/arealWeighting/arealWeighting_source_rel.geojson",
            target="tests/arealWeighting/arealWeighting_target_rel.geojson",
            attribute="rel_val",
            function=cogran.areal_weighting,
            relative=True,
        )

        self.assertAlmostEqual(target_features[0]["properties"]["rel_val"], 0.1)
        self.assertAlmostEqual(target_features[1]["properties"]["rel_val"], 0.1)
        self.assertAlmostEqual(target_features[2]["properties"]["rel_val"], 0.4)
        self.assertAlmostEqual(target_features[3]["properties"]["rel_val"], 0.3)
        self.assertAlmostEqual(target_features[4]["properties"]["rel_val"], 0.2)
        self.assertAlmostEqual(target_features[5]["properties"]["rel_val"], 0.1)


if __name__ == "__main__":
    unittest.main()
